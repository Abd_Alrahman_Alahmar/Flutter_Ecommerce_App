# Ecommerce App (Flutter)

## Features
Email & Password Authentication <br />
Persisting Auth State <br />
Searching Products <br />
Filtering Products (Based on Category) <br />
Product Details <br />
Rating <br />
Getting Deal of the Day <br />
Cart <br />
Checking out with Google Pay <br />
Viewing My Orders <br />
Viewing Order Details & Status <br />
Sign Out <br />
Admin Panel <br />
Viewing All Products <br />
Adding Products <br />
Deleting Products <br />
Viewing Orders <br />
Changing Order Status <br />
Viewing Total Earnings <br />

## Tech used
Server: Node.js, Express, Mongoose, MongoDB, Cloudinary

Client: Flutter, Provider

## ScreenShots

<p align="center">
![ALT](screenshot/SignUp.jpg){width="240" height="500"} 
![ALT](screenshot/SignIn.jpg){width="240" height="500"}
![ALT](screenshot/Main%20Screen.jpg){width="240" height="500"}  
</p>

<p align="center">
![ALT](screenshot/Product%20Detail.jpg){width="240" height="500"} 
![ALT](screenshot/Account.jpg){width="240" height="500"}
![ALT](screenshot/Cart.jpg){width="240" height="500"}  
</p>

<p align="center">
![ALT](screenshot/Order%20Status.jpg){width="240" height="500"} 
![ALT](screenshot/Payment.jpg){width="240" height="500"}
![ALT](screenshot/Admin%20Dashboard.jpg){width="240" height="500"}   
</p>

<p align="center">
![ALT](screenshot/Admin%20Add%20Product.jpg){width="240" height="500"} 
![ALT](screenshot/Sales.jpg){width="240" height="500"}
![ALT](screenshot/Confirming%20Order.jpg){width="240" height="500"}
</p>
  
 

